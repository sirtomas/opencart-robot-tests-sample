*** Settings ***
Resource    ../../Keywords/Common_Keywords.robot
Resource    ../../Keywords/Customer_Login_Page_Keywords.robot
Test Setup    Open Browser    ${appLink}    ${browser}
Test Teardown   Close Browser
Library           Selenium2Library

*** Test Cases ***

Customer Empty Login Test
    Login as Customer     ${EMPTY}    ${EMPTY}
    Wait Until Page Contains    Varování: E-mailová adresa a heslo se neshodují.

Customer Invalid Login Test
    Login As Customer     ${nonExistentUsername}    ${nonExistentPassword}
    Wait Until Page Contains    Varování: E-mailová adresa a heslo se neshodují.

Customer Valid Login Test
    Login As Customer   ${customerUsername}    ${customerPassword}
    Title Should Be    Můj účet
