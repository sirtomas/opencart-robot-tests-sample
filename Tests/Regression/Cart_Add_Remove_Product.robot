*** Settings ***
Resource    ../../Keywords/Common_Keywords.robot
Resource    ../../Keywords/Cart_Page_Keywords.robot
Resource    ../../Objects/Front/Cart_Page.robot
Suite Setup    Load Shop Main Page
Suite Teardown   Close Browser
Library           Selenium2Library

*** Test Cases ***
Add Product To Cart
    Add Product To Cart   ${sampleProductUrl}   1
    Wait Until Page Contains    Úspěch: Přidal(a) jste ${sampleProductName} do Vašeho nákupního košíku!
    Cart Contains Item    ${sampleProductUrl}    1

Remove Product From Cart
    Click Button    ${cartItemRemoveButId}
    Wait Until Page Does Not Contain Element    ${cartItemRemoveButId}
