*** Settings ***
Resource    ../../Keywords/Common_Keywords.robot
Resource    ../../Objects/Front/Search_Results_Page.robot
Test Setup    Load Shop Main Page
Test Teardown   Close Browser
Library           Selenium2Library

*** Test Cases ***
Search Product Test
    Search Product   ${sampleProductName}
    Wait Until Page Contains Element    css=${searchResultsPageContainerSelector}
    Title Should Be    Vyhledávání - ${sampleProductName}
    Page Should Contain Link     ${sampleProductName}
