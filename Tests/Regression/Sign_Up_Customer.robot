*** Settings ***
Resource    ../../Keywords/Common_Keywords.robot
Test Setup    Open Browser    ${signUpPageUrl}    ${browser}
Test Teardown   Close Browser
Library           Selenium2Library
Library     OperatingSystem
Library     String

*** Test Cases ***
Customer Sign Up
  Sign Up New Random Customer
  Wait Until Page Contains    Váš účet byl vytvořen!
