*** Settings ***
Resource    ../../Keywords/Common_Keywords.robot
Resource    ../../Keywords/Contact_Page_Keywords.robot
Test Setup    Load Shop Contact Page
Test Teardown   Close Browser
Library           Selenium2Library

*** Test Cases ***
Submit Message
  Send Message    ${nonExistentName}    ${nonExistentEmail}    ${sampleContactMessage}
  Wait Until Page Contains    Váš dotaz byl úspěšně odeslán administrátorovi obchodu!

Input Validation
  Send Message    ${EMPTY}    ${EMPTY}    ${EMPTY}
  Wait Until Page Contains Element    css=.has-error
  Page Should Contain    Jméno musí mít mezi 3 a 32 znaky!
  Page Should Contain    E-mailová adresa je zřejmě neplatná!
  Page Should Contain    Dotaz musí mít mezi 10 a 3000 znaky!

  Send Message    ab    ${EMPTY}    ${EMPTY}
  Wait Until Page Contains Element    css=.has-error
  Page Should Contain    Jméno musí mít mezi 3 a 32 znaky!
  Page Should Contain    E-mailová adresa je zřejmě neplatná!
  Page Should Contain    Dotaz musí mít mezi 10 a 3000 znaky!

  Send Message    Toto je prilis dlouhe jmeno na to, aby se to odeslalo.    ${EMPTY}    ${EMPTY}
  Wait Until Page Contains Element    css=.has-error
  Page Should Contain    Jméno musí mít mezi 3 a 32 znaky!
  Page Should Contain    E-mailová adresa je zřejmě neplatná!
  Page Should Contain    Dotaz musí mít mezi 10 a 3000 znaky!

  Send Message    ${nonExistentName}    ${EMPTY}    ${EMPTY}
  Wait Until Page Contains Element    css=.has-error
  Page Should Not Contain    Jméno musí mít mezi 3 a 32 znaky!
  Page Should Contain    E-mailová adresa je zřejmě neplatná!
  Page Should Contain    Dotaz musí mít mezi 10 a 3000 znaky!

  Send Message    ${nonExistentName}    abcde   ${EMPTY}
  Wait Until Page Contains Element    css=.has-error
  Page Should Contain    E-mailová adresa je zřejmě neplatná!
  Page Should Contain    Dotaz musí mít mezi 10 a 3000 znaky!

  Send Message    ${nonExistentName}    ${nonExistentEmail}   ${EMPTY}
  Wait Until Page Contains Element    css=.has-error
  Page Should Not Contain    E-mailová adresa je zřejmě neplatná!
  Page Should Contain    Dotaz musí mít mezi 10 a 3000 znaky!

  Send Message    ${nonExistentName}    ${nonExistentEmail}   Malo
  Wait Until Page Contains Element    css=.has-error
  Page Should Contain    Dotaz musí mít mezi 10 a 3000 znaky!

  Send Message    ${nonExistentName}    ${nonExistentEmail}   ${tooLongText}
  Wait Until Page Contains Element    css=.has-error
  Page Should Contain    Dotaz musí mít mezi 10 a 3000 znaky!
