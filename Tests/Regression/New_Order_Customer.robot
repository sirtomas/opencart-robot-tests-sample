*** Settings ***
Resource    ../../Keywords/Common_Keywords.robot
Test Setup    Load Shop Main Page
Test Teardown   Close Browser
Library           Selenium2Library

*** Test Cases ***

New Order Customer
  Login As Customer    ${customerUsername}    ${customerPassword}
  Order Product As Logged Customer    ${sampleProductUrl}    1
  Wait Until Page Contains    Vaše objednávka byla odeslána ke zpracování!
