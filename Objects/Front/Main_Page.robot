*** Settings ***
Resource    ../../Settings/Global_Settings.robot

*** Variables ***
${searchPanel}    #search
${searchInput}    ${searchPanel} input[name="search"]
${searchSubmitButton}    ${searchPanel} button
