*** Settings ***
Resource    ../../Settings/Global_Settings.robot

*** Variables ***
${customerPasswordInputSelector}      input#input-password
${customerUsernameInputSelector}      input#input-email
${customerLoginFormSubmitSelector}       input[value="Přihlásit se"]
${customerLoginPageUrl}      ${appLink}/index.php?route=account/login
