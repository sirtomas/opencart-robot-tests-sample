*** Settings ***
Resource    ../../Settings/Global_Settings.robot

*** Variables ***
${checkoutPageUrl}    ${appLink}/index.php?route=checkout/checkout
${paymentAddressSubmitSelector}   button-payment-address
${shippingAddressSubmitSelector}   button-shipping-address
${shippingMethodSubmitSelector}   button-shipping-method
${termsCheckboxSelector}    //*[@id="collapse-payment-method"]/div/div[2]/div/input[1]
${paymentMethodSubmitSelector}    button-payment-method
${submitOrderSelector}    button-confirm
