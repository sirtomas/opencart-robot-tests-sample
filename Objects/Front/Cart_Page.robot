*** Settings ***
Resource    ../../Settings/Global_Settings.robot

*** Variables ***
${cartPageUrl}    ${appLink}/index.php?route=checkout/cart
${cartItemsCssSelector}   .checkout-cart form table tbody tr
${cartItemQuantitySelector}   .checkout-cart form table tbody tr:first-child td:nth-child(4) div input@value
${cartItemRemoveButId}    //*[@id="content"]/form/div/table/tbody/tr/td[4]/div/span/button[2]
