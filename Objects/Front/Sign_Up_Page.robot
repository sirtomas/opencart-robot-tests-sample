*** Settings ***
Resource    ../../Settings/Global_Settings.robot

*** Variables ***
${signUpPageUrl}   ${appLink}/index.php?route=account/register
${signUpNameSelector}    input#input-firstname
${signUpSurnameSelector}    input#input-lastname
${signUpEmailSelector}    input#input-email
${signUpTelSelector}    input#input-telephone
${signUpStreetSelector}    input#input-address-1
${signUpCitySelector}    input#input-city
${signUpPasswordSelector}    input#input-password
${signUpPasswordConfirmSelector}    input#input-confirm
${signUpTermsSelector}    input[name="agree"]
${zoneSelector}    select#input-zone option[value="899"]
${signUpSumbitSelector}   input[value="Pokračovat"]
