*** Variables ***
${appLink}    https://4it446.tomasfenyk.cz
${loginPageLink}    https://4it446.tomasfenyk.cz/index.php?route=account/login
${browser}    chrome
${sampleProductUrl}   macbook-air
${sampleProductName}   MacBook Air
${customerUsername}   4it446zakaznik@tomasfenyk.cz
${customerPassword}   zakaznikjepan
${nonExistentName}    Jarda Nanic
${nonExistentUsername}    4it446nevim@tomasfenyk.cz
${nonExistentPassword}    zapomneljsemseregistrovat
${sampleContactMessage}    Úplně nevim, co bych sem měl napsat, ale chtěl bych si objednat kamion plnej květináčů.
${nonExistentEmail}   4it446nonexistent@tomasfenyk.cz
${customerName}    Pepa Volek
