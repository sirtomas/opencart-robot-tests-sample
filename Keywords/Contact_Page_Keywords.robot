*** Settings ***
Resource    ../Settings/Global_Settings.robot
Resource    ../Objects/Front/Contact_Page.robot
Resource    ./Common_Keywords.robot

Library     Selenium2Library

*** Keywords ***
Send Message
  [Arguments]   ${name}   ${email}    ${message}
  Input Text    css=${contactFormNameSelector}    ${name}
  Input Text    css=${contactFormEmailSelector}    ${email}
  Input Text    css=${contactFormMessageSelector}    ${message}
  Click Button    css=${contactFormSubmitButtonSelector}
