*** Settings ***
Library    Selenium2Library
Library    String
Library    OperatingSystem
Resource    ../Settings/Global_Settings.robot
Resource    ../Objects/Front/Contact_Page.robot
Resource    ../Objects/Front/Customer_Login_Page.robot
Resource    ../Objects/Front/Main_Page.robot
Resource    ../Objects/Front/Product_Page.robot
Resource    ../Objects/Front/Sign_Up_Page.robot
Resource    ./Contact_Page_Keywords.robot
Resource    ../Objects/Front/Checkout_Page.robot
Resource    ./Checkout_Page_Keywords.robot
Resource    ./Customer_Login_Page_Keywords.robot

*** Keywords ***
Load Shop Main Page
    Open Browser    ${appLink}    ${browser}

Load Shop Contact Page
    Open Browser    ${contactPageUrl}    ${browser}

Load Admin Login Page
    Open Browser    ${adminLoginPageUrl}    ${browser}

Add Product To Cart
    [Arguments]    ${productUrl}    ${quantity}
    Go To    ${appLink}/${productUrl}
    Click Add To Cart Product Page

Order Product As Logged Customer
    [Arguments]    ${productUrl}    ${quantity}
    Add Product To Cart    ${productUrl}    ${quantity}
    Complete Order As Logged Customer

Click Add To Cart Product Page
    Click Button    css=${productPageAddToCartButton}

Search Product
    [Arguments]    ${productName}
    Input Text    css=${searchInput}    ${sampleProductName}
    Press Key    css=${searchInput}    \\13

Sign Up New Random Customer
    ${randomString}=   Generate Random String    5    [LOWER]
    ${newUserName}=   Set Variable    4it446zak-${randomString}@tomasfenyk.cz
    Create File    ${CURDIR}${/}sign_up_test_users.txt    ${newUserName}${\n}
    Go To    ${signUpPageUrl}
    Input Text    css=${signUpNameSelector}    ZakaznikBot
    Input Text    css=${signUpSurnameSelector}    ${randomString}
    Input Text    css=${signUpEmailSelector}    ${newUserName}
    Input Text    css=${signUpTelSelector}    +420 123 456 789
    Input Text    css=${signUpStreetSelector}    Nikde 123
    Input Text    css=${signUpCitySelector}    Zelena louka
    Click Element    css=${zoneSelector}
    Input Password    css=${signUpPasswordSelector}    ${customerPassword}
    Input Password    css=${signUpPasswordConfirmSelector}    ${customerPassword}
    Select Checkbox    css=${signUpTermsSelector}
    Click Button    css=${signUpSumbitSelector}

Get Product Price    [Arguments]    ${productUrl}    ${priceElementLoc}
    Go To    ${productUrl}
    Wait Until Page Contains Element    ${priceElementLoc}
    ${price} =     Get Text    ${priceElementLoc}
    [Return]    ${price}
