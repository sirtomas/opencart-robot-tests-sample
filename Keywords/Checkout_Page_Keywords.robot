*** Settings ***
Resource          ../Settings/Global_Settings.robot
Resource          ../Keywords/Common_Keywords.robot
Resource          ../Keywords/Customer_Login_Page_Keywords.robot
Library           Selenium2Library

*** Keywords ***
Complete Order As Logged Customer
  Go To    ${checkoutPageUrl}
  Wait Until Element Is Visible    ${paymentAddressSubmitSelector}
  Click Button    ${paymentAddressSubmitSelector}
  Wait Until Element Is Visible    ${shippingAddressSubmitSelector}
  Click Button    ${shippingAddressSubmitSelector}
  Wait Until Element Is Visible    ${shippingMethodSubmitSelector}
  Click Button    ${shippingMethodSubmitSelector}
  Wait Until Element Is Visible    ${termsCheckboxSelector}
  Select Checkbox    ${termsCheckboxSelector}
  Wait Until Element Is Visible    ${paymentMethodSubmitSelector}
  Click Button    ${paymentMethodSubmitSelector}
  Wait Until Element Is Visible    ${submitOrderSelector}
  Click Button    ${submitOrderSelector}
