*** Settings ***
Resource          ../Objects/Front/Customer_Login_Page.robot
Resource          ../Objects/Front/Customer_Account_Page.robot
Library           Selenium2Library

*** Keywords ***
Login As Customer
  [Arguments]   ${username}   ${pass}
  Go To    ${loginPageLink}
  Maximize Browser Window
  Input Text    css=${customerUsernameInputSelector}    ${username}
  Input Password     css=${customerPasswordInputSelector}    ${pass}
  Click Button    css=${customerLoginFormSubmitSelector}
  Wait Until Page Contains Element    ${myAccountMenuIdXpathSelector}
