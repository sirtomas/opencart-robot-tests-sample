*** Settings ***
Resource    ../Settings/Global_Settings.robot
Resource    ./Common_Keywords.robot
Resource    ../Objects/Front/Cart_Page.robot
Library     Selenium2Library

*** Keywords ***
Cart Contains Item
  [Arguments]   ${itemLink}   ${quantity}
  Go To    ${cartPageUrl}
  Page Should Contain Link    css=a[href="${appLink}/${itemLink}"]

  ${givenQuantity}=   Get Element Attribute    css=${cartItemQuantitySelector}
  Should Match    ${givenQuantity}    ${quantity}
